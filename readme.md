# DucoPay Python example

This is a example on how to use DucoPay with Python and Flask.   
It is a "Lipsum store" - user enters his email address -> is redirected to DucoPay payment page -> in background we are processing payment if it was successful (updating record in database) -> redirecting user to Thank You page

   
This example is using sqlite3 as simple database system and .env file. Before usage, you need to create store on DucoPay website, and enter API key and shopID to .env file
## Setup shop on DucoPay
[How to create shop on DucoPay](https://issei.space/projects/ducopay/docs/quickstart/)   
In callback URL enter `https://[yourURL]/callback` and on Thank You page `https://[yourURL]/thankyou`

## How to run
Rename .env.example to .env and change values of API key and ShopID, then launch flask app.   

```
pip3 install -r requirements.txt
export FLASK_APP=app.py
flask run
```
Note - you need to have this website available on internet for callback to work. For local development you can use reverse SSH proxy or service like ngrok
