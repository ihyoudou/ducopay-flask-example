from dotenv import load_dotenv
from flask import Flask,redirect,request
import requests
import hashlib
import sqlite3
import base64
import json
import hmac
import time
import os
import re

app = Flask(__name__)
load_dotenv() # loading .env file

con = sqlite3.connect(os.environ.get("sqliteDBname")) # loading database
cur = con.cursor()

# Create table if it doesn't exist
cur.execute('''CREATE TABLE IF NOT EXISTS orders
               (date text, email text, status text)''')
con.commit()
con.close()


@app.route("/")
def hello_world():
        return '''
              <h3>Welcome to Lipsum store!</h3>
              <p>Get lorem ipsum straight to your email box!</p>
              <form method="POST" action="/process">
                  <div><label>Your Email: <input type="email" name="email"></label></div>
                  <input type="submit" value="Submit">
              </form>'''

@app.route("/process", methods=['POST'])
def processPage():
    # on this page we are processing data from form (email input) and
    # creating a payment URL
    if request.form.get('email'):
        email = request.form.get('email')

        # Validating if given value is looking like a real email address
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b' 
        if(re.fullmatch(regex, email)):

            con = sqlite3.connect(os.environ.get("sqliteDBname")) # loading database
            cur = con.cursor()
            # Insert a row of data
            cur.execute("INSERT INTO orders VALUES ('{}','{}','created')".format(time.time(), email))
            
            con.commit() # Save (commit) the changes
            
            lastrowID = cur.lastrowid # Getting rowID of that record
            con.close()

            url = "https://pay.duinocoin.com/api/v1/createTransaction"

            payload = {
                "apikey": os.environ.get("ducopayAPIkey"),
                "shopID": os.environ.get("ducopayShopID"),
                "amount": 0.1234,
                "operationNumber": lastrowID,
                "description": "Lorem ipsum payment"
            }
            headers = {"Content-Type": "application/json"}

            response = requests.request("POST", url, json=payload, headers=headers)
            responseJSON = json.loads(response.text)
            if responseJSON['success'] == True:
                # if response from DucoPay was successfull - redirecting to payment page
                return redirect(("https://pay.duinocoin.com/pay/{}/{}")
                    .format(
                        os.environ.get("ducopayShopID"), 
                        responseJSON['uid']
                    ), code=302)
        else:
            return "<h3>Invalid email</h3>"

    

    

@app.route("/thankyou")
def thankYouPage():
    return "<h3>Thank you for purchase in our store!</h3>"

@app.route("/callback", methods=['POST'])
def callbackHandler():
    # processing json body
    data = json.loads(request.data)
    # checking if ducopay returned 'success'=>true
    if data['success'] == True:
        # creating our signature from data
        combained = str(data['opNumber'])+str(data['transactionID'])
        signature = base64.b64encode( # encoding result of sha256 into base64
                                    hmac.new(
                                            bytes(os.environ.get("ducopayAPIkey"), encoding='utf8'), # our API key encoded to bytes (required by hmac)
                                            combained.encode('utf-8'), # string of combined opNumber and transactionID
                                            hashlib.sha256 # we will encode it using sha256
                                            )
                                        .hexdigest()
                                        .encode('utf-8')
                                    )

        # checking if our encoded signature coresponse with recived signature
        if(signature.decode("utf-8") == data['signature']):
            # do whatever after successful verification

            con = sqlite3.connect(os.environ.get("sqliteDBname")) # loading database
            cur = con.cursor()
            # Updating db record with transaction
            cur.execute("UPDATE orders SET status='completed' WHERE ROWID={}".format(data['opNumber']))
            con.commit() # Save (commit) the changes
            con.close() # Closing database connection


            return "ok" # callback doesnt need to return any value - this is just for demo
        else:
            return "ko"

    # if return from ducopay was 'success'=>false
    else:
        return "ko"
